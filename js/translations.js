const translations = {
    en: {
        home: "Home",
        about: "About",
        skills: "Skills",
        projects: "Projects",
        services: "Services",
        contact: "Contact",
        hello: "Hello, I'm",
        name: "Khanh",
        roles: "Web Developer, Apps Developer, Freelancer",
        contactNow: "Contact Now",
        yearsExperience: "Years of Web/App Development Experience",
        welcomeMessage: "Welcome to my world!",
        aboutMe: "I am a freelance developer with over 8 years of experience in programming and application development. Professionalism and dedication are my top priorities, and I take pride in my ability to work independently and solve problems creatively. I have worked on various projects, from mobile applications to computer software. My skills include web programming, system analysis, and database management. I am committed to delivering effective and high-quality technology solutions for each client.",
        affordablePricing: "Affordable pricing",
        highQualityProducts: "High-quality products",
        onTimeDelivery: "On-time project delivery",
        freeWarranty: "1-year free warranty",
        satisfiedCustomers: "Satisfied Customers",
        completedProjects: "Completed Projects",
        customerSatisfaction: "We take pride in our customer satisfaction. Each project we complete brings joy and satisfaction to our customers. This is the result of our team's dedication, professionalism, and commitment to each client.",
        projectCompletion: "Each completed project is not only the result of effort and passion but also a testament to our commitment and professionalism. We are proud of every product delivered and the satisfaction it brings to our customers.",
        webAppDevelopment: "Website and Mobile App Development",
        startingPrice: "Starting from",
        webAppDescription: "Our website programming service provides customized and high-quality solutions to boost your business's online presence. Leveraging a combination of creativity and technology, we build beautiful, flexible, and device-optimized websites to help you attract and retain customers.",
        uiDesign: "Interface Design",
        uiDescription: "We specialize in designing beautiful and effective interfaces for web and mobile applications. With a focus on user experience, we create innovative, user-friendly, and interactive interfaces that enhance user engagement and attract users.",
        featuredProjects: "Featured Projects",
        allProjects: "All Projects",
        webDevelopment: "Web Development",
        mobileDevelopment: "Mobile Development",
        contactInfo: "Contact Information",
        office: "Office",
        phone: "Phone",
        email: "Email",
        sendMessage: "Send Message",
        yourName: "Your Name",
        yourEmail: "Your Email",
        subject: "Subject",
        message: "Message"
    },
    vi: {
        home: "Trang chủ",
        about: "Về tôi",
        skills: "Kỹ năng",
        projects: "Dự án",
        services: "Dịch vụ",
        contact: "Liên hệ",
        hello: "Xin chào, tôi là",
        name: "Khánh",
        roles: "Web Developer, Apps Developer, Freelancer",
        contactNow: "Liên hệ ngay",
        yearsExperience: "Năm kinh nghiệm lập trình Web/App",
        welcomeMessage: "Chào mừng bạn đến với thế giới của tôi!",
        aboutMe: "Tôi là một freelancer developer với hơn 8 năm kinh nghiệm trong lập trình và phát triển ứng dụng. Sự chuyên nghiệp và tận tâm đặt lên hàng đầu, tôi tự hào về khả năng làm việc độc lập và giải quyết vấn đề một cách sáng tạo. Tôi đã làm việc trên nhiều dự án đa dạng, từ ứng dụng di động đến phần mềm máy tính. Kỹ năng của tôi bao gồm lập trình web, phân tích hệ thống và quản lý cơ sở dữ liệu. Tôi cam kết đem lại giải pháp công nghệ hiệu quả và chất lượng cho mỗi khách hàng.",
        affordablePricing: "Giá cả phải chăng",
        highQualityProducts: "Sản phẩm chất lượng cao",
        onTimeDelivery: "Giao dự án đúng thời hạn",
        freeWarranty: "Bảo hành miễn phí 1 năm",
        satisfiedCustomers: "Khách hàng hài lòng",
        completedProjects: "Dự án đã hoàn thành",
        customerSatisfaction: "Chúng tôi tự hào về sự hài lòng của khách hàng. Mỗi dự án mà chúng tôi hoàn thành đều đem lại niềm vui và sự hài lòng cho khách hàng. Điều này là kết quả của sự tận tâm, chuyên nghiệp và sự cam kết của đội ngũ chúng tôi đối với mỗi khách hàng.",
        projectCompletion: "Mỗi dự án đã hoàn thành không chỉ là kết quả của sự nỗ lực và tâm huyết mà còn là minh chứng cho sự cam kết và chuyên nghiệp của chúng tôi. Chúng tôi tự hào về mỗi sản phẩm đã được giao và sự hài lòng mà nó mang lại cho khách hàng của chúng tôi.",
        webAppDevelopment: "Lập trình website, App di động",
        startingPrice: "Giá khởi điểm từ",
        webAppDescription: "Dịch vụ lập trình website của chúng tôi cung cấp giải pháp tùy chỉnh và chất lượng cao để thúc đẩy sự hiện diện trực tuyến của doanh nghiệp bạn. Tận dụng sự kết hợp giữa sự sáng tạo và kỹ thuật, chúng tôi xây dựng các trang web đẹp mắt, linh hoạt và tối ưu hóa cho mọi thiết bị, giúp bạn thu hút và giữ chân khách hàng.",
        uiDesign: "Thiết kế giao diện",
        uiDescription: "Chúng tôi chuyên về thiết kế giao diện đẹp mắt và hiệu quả cho các ứng dụng web và di động. Với sự tập trung vào trải nghiệm người dùng, chúng tôi tạo ra các giao diện sáng tạo, dễ sử dụng và tương tác, giúp tăng cường tương tác và thu hút người dùng.",
        featuredProjects: "Các dự án tiêu biểu",
        allProjects: "Tất cả dự án",
        webDevelopment: "Phát triển Web",
        mobileDevelopment: "Ứng dụng Di động",
        contactInfo: "Thông tin liên hệ",
        office: "Văn phòng",
        phone: "Điện thoại",
        email: "Email",
        sendMessage: "Gửi tin nhắn",
        yourName: "Tên của bạn",
        yourEmail: "Email của bạn",
        subject: "Chủ đề",
        message: "Tin nhắn"
    }
};

function setLanguage(lang) {
    document.querySelectorAll('[data-translate]').forEach(element => {
        const key = element.getAttribute('data-translate');
        if (translations[lang][key]) {
            element.textContent = translations[lang][key];
        }
    });
    localStorage.setItem('language', lang);
}

document.addEventListener('DOMContentLoaded', () => {
    const savedLang = localStorage.getItem('language') || 'vi';
    setLanguage(savedLang);

    document.querySelectorAll('.dropdown-item[data-lang]').forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault();
            setLanguage(e.target.getAttribute('data-lang'));
        });
    });
});